This project is an effort to create a responsive table with the following requirements:

* Responsive ;) The idea would be to allow the following configuration
    - Relative column size
    - Fixed column size
* Pin column
* Grouping of column header
* Edit mode
* Alignment of content
* Sorting
* Filtering
* Mobile support
* Configurable Layout using a dialog:
    * Show/hide
    * Editable/Readonly/
    * Reordering of column
    * Column Size
* Configurable Layout with inline edit mode

* Touch and DnD support

## Development
This project is built with Lerna.